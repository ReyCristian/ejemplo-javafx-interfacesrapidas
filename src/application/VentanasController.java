package application;

import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class VentanasController {

	@FXML
	private BorderPane panel1;
	@FXML
	private BorderPane panel2;
	@FXML
	private BorderPane panel3;
	private Stage ventana;
	private Scene vista3;
	private Scene vista1;
	private Scene vista2;
	
	@FXML
	public void initialize() {
		crearVentana();
		vista1=new Scene(panel1,600,400);
		vista2=new Scene(panel2,600,400);
		vista3=new Scene(panel3,600,400);
		cargarVista(vista1);
		
	}
	
	private void cargarVista(Scene v) {
		ventana.setScene(v);
		
	}

	private void crearVentana() {
		ventana = new Stage();
		ventana.setScene(new Scene(new AnchorPane()));
		ventana.show();
	}

	@FXML
	public void abrirVista1() {
		cargarVista(vista1);
	}	
	@FXML
	public void abrirVista2() {
		cargarVista(vista2);
	}	
	@FXML
	public void abrirVista3() {
		cargarVista(vista3);
	}
	
	
}
